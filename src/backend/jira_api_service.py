from jira import JIRA
import build_data_model as Model
import constants

#Retrieve all the issues linked to the given issue
def get_all_issues(connected_jira, issue):
    issues = []
    chunk = connected_jira.search_issues(f'issue in linkedIssues({issue})', startAt=0, maxResults=False, fields=constants.DEFAULT_TICKET_FIELDS)
    issues += chunk.iterable
    return issues

#return all issues in requested epic
def get_all_issues_in_epic(connected_jira, epic):
    model = {}
    issues = []
    built_issues = []
    i = constants.ZERO
    #retrieve issues in chunks of 50 until all are found
    while True:
        chunk = connected_jira.search_issues(f'"Epic Link" = {epic}', startAt=i, maxResults=constants.CHUNK_SIZE, fields=constants.DEFAULT_TICKET_FIELDS)
        i += constants.CHUNK_SIZE
        issues += chunk.iterable
        if i >= chunk.total:
            break
    for issue in issues:
        #if str(issue.fields.status) == 'To Do' or str(issue.fields.status) == 'In Progress' or str(issue.fields.status) == 'Code Review' or str(issue.fields.status) == 'Merged':
        built_issue = Model.build_data_model(issue)
        built_issues.append(built_issue)
    for issue in built_issues:
        print(issue['key'])

    model['issues'] = built_issues
    return model

#return the jira issue with matching issue_key
def get_issue(connected_jira, issue_key):
    return connected_jira.issue(issue_key, constants.DEFAULT_TICKET_FIELDS)

#Add an issuelink relation of specified type, and the issues involved
def create_link(connected_jira, link_type, link_from, link_to):
    link_from_issue = get_issue(connected_jira, link_from)
    link_to_issue = get_issue(connected_jira, link_to)

    if(link_from_issue is not None and link_to_issue is not None and link_type in constants.LINK_TYPES):
        result = connected_jira.create_issue_link(link_type, link_from, link_to)
        return result.status_code

    return 400

#assign a user to an issue from the app. Currently has permission issues in JIRA
def assign(connected_jira, issue_id, user):
    result = connected_jira.assign_issue(issue_id, user)
    return result.status_code

#update issue issue_id with the fields_with_new_values Dict
#assignee is a complex and detailed field that requires its own call
def update_issue(connected_jira, issue_id, fields_with_new_values):
    if constants.ASSIGNEE in fields_with_new_values:
        connected_jira.assign_issue(issue_id, fields_with_new_values[constants.ASSIGNEE])
        del fields_with_new_values[constants.ASSIGNEE]

    issue = connected_jira.issue(issue_id)
    result = issue.update(fields=fields_with_new_values)

    return 200

#return all active issues of the requested project
def get_project_issues(connected_jira, project_id):
    model = {}
    issues = []
    built_issues = []
    i = constants.ZERO
    #retrieve issues in chunks of 50 until all are found
    while True:
        chunk = connected_jira.search_issues(f'project = {project_id}', startAt=i, maxResults=constants.CHUNK_SIZE, fields=constants.DEFAULT_TICKET_FIELDS)
        i += constants.CHUNK_SIZE
        issues += chunk.iterable
        if i >= chunk.total:
            break
    for issue in issues:
        if str(issue.fields.status) == 'To Do' or str(issue.fields.status) == 'In Progress' or str(issue.fields.status) == 'Code Review' or str(issue.fields.status) == 'Merged':
            built_issue = Model.build_data_model(issue)
            built_issues.append(built_issue)
    for issue in built_issues:
        print(issue['key'])

    model['issues'] = built_issues
    return model
