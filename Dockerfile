#docker run command in cmd to open up the proper port
#docker run --rm -p 5000:5000 -p 3000:3000 vidmob/ticket_dependency_viewer:001
#if startapp.sh file is not found, it is because of Windows line endings. Open file
#in notepad++ or other text editor and convert line endings to unix

FROM mhart/alpine-node:12.21.0

RUN apk update && apk add --no-cache bash gcc python3 py3-pip python3-dev build-base make
RUN apk add --no-cache libressl-dev musl-dev libffi-dev openssl-dev


# install packages needed for the backend.
RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir cryptography==2.1.4
RUN pip3 install python-dotenv
RUN pip3 install jira
RUN pip3 install requests
RUN pip3 install flask
RUN pip3 install flask_restful
RUN pip3 install flask_cors

RUN apk del libressl-dev musl-dev libffi-dev

EXPOSE 5000
EXPOSE 3000

COPY src/ ./src
RUN chmod 500 /src/startapp.sh

WORKDIR /src/frontend
RUN npm ci
WORKDIR /src

ENTRYPOINT ["./startapp.sh"]
