import { createContext } from "react";

/* Add project and board context to avoid props drilling when passing board and project from the 
project and board select page to the ticket visualization page */
const selectionContext = createContext({
  ticket: {ticketId: '', depth: 2},
  setTicket: (ticket) => {}
});

export default selectionContext;