// Verify if the user uses a VidMob email or not
export const verifyEmail = (email) => {
  if (!email.includes('@vidmob.com')) {
    alert('Please use your VidMob email!');
    return false;
  }
  return true;
}