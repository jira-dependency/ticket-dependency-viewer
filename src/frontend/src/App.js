import React, { useState } from 'react';
import './App.css';
import Routes from './components/Routes';
import { selectionContext } from './utils/context';
import * as constants from './utils/constants';

function App() {
  const [selectedTicket, setSelectedTicket] = useState({
    ticketId: '',
    depth: constants.MIN_DEPTH
  });

  return (
    <div className="App">
      <selectionContext.Provider value={{ selectedTicket, setSelectedTicket }}>  
        <Routes />
      </selectionContext.Provider>
    </div>
  );
}

export default App;
