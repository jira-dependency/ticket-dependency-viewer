import React from 'react';
import {
  Switch,
  Route
} from "react-router-dom";
import { Home } from '../pages/Home';
import { Visualization } from '../pages/Visualization';
import { ProjectSelect } from '../pages/ProjectSelect';

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/visualization">
        <Visualization />
      </Route>
      <Route path="/project-select">
        <ProjectSelect />
      </Route>
    </Switch>
  );
}

export default Routes;