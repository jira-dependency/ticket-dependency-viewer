import React from 'react';
import { Row, Col } from 'antd';
import * as constants from '../../utils/constants';
import './ColorNodeTable.css';

// This component is a status - color reference table
const ColorNodeTable = () => {
  const COL_SPAN = 12;

  return (
    <Col span={COL_SPAN}>
      <div className="type-headline-80">
        Status color
      </div>
      <div className="color-node-table-container">
        <Row>
          <Col span={COL_SPAN}>
            <div className="type-headline-90">Status</div>
          </Col>
          <Col span={COL_SPAN}>
            <div className="type-headline-90">Color</div>
          </Col>
        </Row>
        {constants.STATUS_COLOR.map((status) => {
          let issueStatus = status.status.join(', ');
          return (
            <Row className="color-table-row">
              <Col span={COL_SPAN}>
                <div className="type-body-default">{issueStatus}</div>
              </Col>
              <Col span={COL_SPAN} className="color-container">
                <div className={status.className}>
                  <span>{status.color}</span>
                </div>
              </Col>
            </Row>
          );
        })}
      </div>
    </Col>
  );
};

export default ColorNodeTable;