import build_data_model as Model
import jira_api_service as JiraApi
import constants
import connection
import constants
from timeit import default_timer as timer

#All issuelink types/names
INWARD = 'inwardIssue'
OUTWARD = 'outwardIssue'
BLOCKS = 'Blocks'
CLONES = 'Cloners'
DUPLICATE = 'Duplicate'
RELATES = 'Relates'
ISSUE_SPLIT = 'Issue split'
CAUSES = 'Problem/Incident'
RELEASES = 'Release'
BREAKS_OUT = 'Story breakdown'
SOLVING = 'Support Resolution'
TESTING = 'Testing'

KEY = 'key'
TYPE = 'type'
NAME = 'name'
EMAIL = 'emailAddress'
DISPLAYNAME = 'displayName'
STATE = 'state'
BOARDID = 'boardId'

#create and return a list of every relation an issue has
def get_relationship_lists(field_data):

    blocks = get_links(field_data, BLOCKS, OUTWARD)
    is_blocked_by = get_links(field_data, BLOCKS, INWARD)
    clones = get_links(field_data, CLONES, OUTWARD)
    is_cloned_by = get_links(field_data, CLONES, INWARD)
    duplicates = get_links(field_data, DUPLICATE, OUTWARD)
    is_duplicated_by = get_links(field_data, DUPLICATE, INWARD)
    relates_to = get_links(field_data, RELATES, INWARD)
    split_to = get_links(field_data, ISSUE_SPLIT, OUTWARD)
    split_from = get_links(field_data, ISSUE_SPLIT, INWARD)
    causes = get_links(field_data, CAUSES, OUTWARD)
    caused_by = get_links(field_data, CAUSES, INWARD)
    releases = get_links(field_data, RELEASES, OUTWARD)
    released_by = get_links(field_data, RELEASES, INWARD)
    breaks_out_to = get_links(field_data, BREAKS_OUT, OUTWARD)
    broken_out_from = get_links(field_data, BREAKS_OUT, INWARD)
    solves = get_links(field_data, SOLVING, OUTWARD)
    solved_by = get_links(field_data, SOLVING, INWARD)
    tests = get_links(field_data, TESTING, OUTWARD)
    tested_by = get_links(field_data, TESTING, INWARD)

    LISTS = [blocks, is_blocked_by, clones, is_cloned_by, duplicates, is_duplicated_by,
            relates_to, split_to, split_from, causes, caused_by, releases, released_by,
            breaks_out_to, broken_out_from, solves, solved_by, tests, tested_by]
    return LISTS


#return a list of issues that are related via link_type and link_direction
def get_links(field_data, link_type, link_direction):
    link_list = []
    for link in field_data:
        if link[TYPE][NAME] == link_type and (link_direction in link):
            link_list.append(link[link_direction][KEY])
    return link_list

#get and insert priority of ticket into model
def get_priority(field_data):
    return field_data[NAME]

#get and insert assignee email of ticket into model
def get_assignee(field_data):
    return field_data[DISPLAYNAME]

def get_email(field_data):
    return field_data[EMAIL]

#get and insert status of ticket into model
def get_status(field_data):
    return field_data[NAME]

#get and insert sprint and board that ticket is a part of
def get_sprint(field_data):
    sprint_data = []
    sprint_data.append(field_data[0][NAME])
    sprint_data.append(field_data[0][STATE])
    return sprint_data

#return the boardId that the current issue is a part of
def get_board(field_data):
    return field_data[0][BOARDID]

#return the type of the current issue
def get_type(field_data):
    return field_data[NAME]

#return the Story Points of the current issue
def get_points(field_data):
    if field_data is not None:
        return int(field_data)

#return the requested 'key' issue
def get_requested_issue(jira, key, proj_id):
    issues = ISSUES.get_all_issues(jira, proj_id)
    for issue in issues:
        if(issue.key == key):
            return issue

    print("Was unable to verify issue")
    return

#Find every relation at full Depth e.g Issue1 -> Issue2 -> Issue3 etc...
#without having duplicates
def get_all_related_issues(key, depth=5):
    #requested depth must be increased by 1 to match function format
    depth = depth + 1

    depth_counter = 0
    model = {}
    buffer = [key]
    depth_buffer = []
    seen = []
    built_issues = []

    while depth_counter < depth:
        #get and build selected item from buffer, append to built_issues
        selected_issue = JiraApi.get_issue(connection.jira, buffer[0])
        built_issue = Model.build_data_model(selected_issue)
        built_issues.append(built_issue)
        seen.append(selected_issue.key)

        #Keep 2-depths-at-once optimization, but if the depth is odd then we go by 2
        #until we get to depth - 1, when we will then grab the last depth by itself
        if (depth - depth_counter != 1) and (depth != 1):
            #get all linked issues to selected issue in one call
            jira_issues = JiraApi.get_all_issues(connection.jira, buffer[0])

            #build and append all linked issues from above
            for issue in jira_issues:
                if issue.key in buffer:
                    buffer.remove(issue.key)
                if issue.key in depth_buffer:
                    depth_buffer.remove(issue.key)
                if issue.key not in seen:
                    built_issue = Model.build_data_model(issue)
                    built_issues.append(built_issue)
                    seen.append(issue.key)

            #check for any new issues found that have not been visited yet
            #and append them to our buffer for processing
            for built_issue in built_issues:
                for relation_list in built_issue[constants.ISSUELINKS]:
                    for issue_id in built_issue[constants.ISSUELINKS][relation_list]:
                        if(issue_id not in seen and issue_id not in buffer and issue_id not in depth_buffer and issue_id != key):
                            depth_buffer.append(issue_id)
        #delete first issue in buffer since it has been built and appended
        del buffer[0]
        if len(depth_buffer) == 0 and len(buffer) == 0:
            break
        if len(buffer) == 0:
            depth_counter += 2
            buffer = depth_buffer
            depth_buffer = []

    for i in range (len(built_issues) - 1):
        built_issues[i][constants.POINTS_BLOCKED] = points_blocked(built_issues[i], built_issues)
        built_issues[i][constants.LONGEST_CHAIN_BLOCKED] = longest_chain_blocked(built_issues[i], built_issues)

    for issue in built_issues:
        issue[constants.POINTS_BLOCKED] = points_blocked(issue, built_issues)
        issue[constants.LONGEST_CHAIN_BLOCKED] = longest_chain_blocked(issue, built_issues)

    model['all related issues'] = built_issues
    return model

def points_blocked(built_issue, built_issues):
    buffer = [built_issue[constants.KEY]]
    seen = []
    points_blocked = 0
    while len(buffer) > 0:
        first_issue = get_issue_by_key(buffer[0], built_issues)
        if first_issue is not None:
            for issue in first_issue['issuelinks'][constants.BLOCKS]:
                if issue not in seen:
                    seen.append(issue)
                    buffer.append(issue)
                    issue_data = get_issue_by_key(issue, built_issues)
                    if issue_data is not None:
                        if issue_data['story points'] is not None:
                            points_blocked += issue_data['story points']
        del buffer[0]

    return points_blocked

#go through buffer and make a second list of generated issues
#put those issues in 'seen'

#make generated set the buffer
#repeat x number of times
#x is longest chain

def longest_chain_blocked(built_issue, built_issues):
    buffer = [built_issue[constants.KEY]]
    #the next set of issues used to find further depths
    generator_buffer = []
    #keep track of what has already been used on the graph
    seen = [built_issue[constants.KEY]]
    more_depth_left = True
    min_depth_left = 0
    length_of_chain = 0

    while more_depth_left:
        for each in buffer:
            first_issue = get_issue_by_key(each, built_issues)
            if first_issue is not None:
                for issue in first_issue['issuelinks'][constants.BLOCKS]:
                    if issue not in seen:
                        min_depth_left = 1
                        seen.append(issue)
                        generator_buffer.append(issue)

        if min_depth_left == 0:
            more_depth_left = False
        else:
            length_of_chain += 1
            min_depth_left = 0
        buffer = generator_buffer
        generator_buffer = []
    return length_of_chain


def get_issue_by_key(key, built_issues):
    for issue in built_issues:
        if issue[constants.KEY] == key:
            return issue
    else:
        return None
