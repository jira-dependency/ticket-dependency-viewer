/* 
  This component is used to render the searched ticket relationship. What this component does are:
  - Render the relationship of the searched ticket under a graph format
  - Color code each node in the graph based on the current status of the ticket that represents that node
  - Add different edge styles based on the relationship of tickets with the searched ticket
  - Allow user to search for another ticket 
*/
import React, { useState, useContext, useEffect } from 'react';
import "../pages.css";
import "./Visualization.css";
import Graph from "react-graph-vis";
import { Row, Col } from 'antd';
import selectionContext from '../../utils/context/selectionContext';
import { ProjectSelectionForm } from '../../components/ProjectSelectionForm';
import axios from 'axios';
import { ColorNodeTable } from '../../components/ColorNodeTable';
import { LineTypesTable } from '../../components/LineTypesTable';
import { generateEdgeList } from './util/edgeUtil';
import { generateNodeList } from './util/nodeUtil';
import { TicketEditModal } from '../../components/TicketEditModal';
import { VDSButton, VDSLoading } from '@vidmob/vidmob-design-system';

const Visualization = () => {
  const { selectedTicket, setSelectedTicket } = useContext(selectionContext);
  /*
      isLoading state is used to trigger React to re-render when all of the 
    generateNodes promises resolve
  */
  const [isLoading, setIsLoading] = useState(true);
  /* 
    isValid state is used to keep track of the validity of the ticket ID that the user put in
  */
  const [isValid, setIsValid] = useState(true);
  
  // Placeholder value
  const [nodeList, setNodeList] = useState([]);
  const [edgeList, setEdgeList] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedNodeId, setSelectedNodeId] = useState('');
  const [selectedNode, setSelectedNode] = useState(null);
  const [selectedTicketId, setSelectedTicketId] = useState(0);
  const [network, setNetwork] = useState(null);

  // Ant Design grid layout allows up to 24 columns per row
  const TICKET_LIST_SPAN = 2;
  const GRAPH_SPAN = 24 - TICKET_LIST_SPAN;

  const showModal = () => {
    setIsModalVisible(true);
  };

  /*  
    Fetch the information from the parameter ticket id
    params:
      - ticketID (string): the ticket id that the user is searching for
  */
  const fetchTicket = (ticket) => {
    axios.post('http://local.vidmob.com:5000/fullDepth', {
      ticket: ticket.ticketId,
      depth: ticket.depth
    })
      .then((res) => {
        setIsValid(true);
        generateGraph(res.data["all related issues"]);
      })
      .catch((err) => {
        setIsValid(false);
        setIsLoading(false);
      });
  }

  const generateGraph = (ticketsArr) => {
    const ticketNodesMap = new Map();
    const ticketList = generateNodeList(ticketsArr, ticketNodesMap);
    const edgesList = generateEdgeList(ticketsArr, ticketNodesMap);
    setNodeList(ticketList);
    setEdgeList(edgesList);
    setGraph({
      nodes: ticketList,
      edges: edgesList,
    });
    setIsLoading(false);
  };

  useEffect(() => {
    fetchTicket(selectedTicket);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedTicket.ticketId, selectedTicket.depth]);

  // Since the options for node color and edge color only accept string and object
  const EDGE_COLOR = "#000000";
  const NODE_COLOR = "#ffffff";

  const options = {
    autoResize: false,
    width: '1200px',
    height: '640px',
    layout: {
      improvedLayout:true,
      hierarchical: {
        enabled: false,
        nodeSpacing: 600
      }
    },
    edges: {
      color: EDGE_COLOR
    },
    nodes: {
      color: NODE_COLOR
    },
    /* This helps the graph to generate tickets that do not overlap each other, and makes
    the edges not bouncy */
    physics: {
      barnesHut: {
        springConstant: 0,
        avoidOverlap: 0.5
      }
    }
  };

  const [graph, setGraph] = useState({
    nodes: nodeList,
    edges: edgeList
  });

  const events = {
    selectNode: function(event) {
      let { nodes } = event;
      if (nodes.length > 0) {
        const node = nodeList.find((val) => val.id === nodes[0]);
        const nodeId = node.label.split('\n')[0].split(' ')[0];
        setSelectedTicketId(nodes[0]);
        setSelectedNode(node);
        setSelectedNodeId(nodeId);
        showModal();
      }
    }
  };

  /* 
    It highlights the selected node on the graph
    params: 
      - selectedNode: The selected node object 
  */
  const handleSelectedNode = (selectedNode) => {
    network.view.focus(selectedNode.id);
  }

  const setNetworkInstance = nw => {
    setNetwork(nw);
  };

  const updateGraph = (g) => {
    network.setData(g);
  }

  const onFinish = (values) => {
    setIsLoading(true);
    const ticket = {
      ticketId: values.ticketId,
      depth: parseInt(values.depth)
    }
    setSelectedTicket(ticket);
  };

  const renderValidTicket = () => {
    return (
      <Row>
        <Col span={TICKET_LIST_SPAN}>
          <div>
            <h2>Ticket list</h2>
            <div className="ticket-list-container">
              {nodeList.map((node) => {
                const ticketIdAndIcon = node.label.split('\n');
                const ticketId = ticketIdAndIcon[0].split(' ')[0];
                return (
                  <Row key={node.id} className='ticket-row'>
                    <Col>
                      <VDSButton type="basic" label={ticketId} onClick={() => handleSelectedNode(node)} />
                    </Col>
                  </Row>
                );
              })}
            </div>
          </div>
        </Col>
        <Col span={GRAPH_SPAN}>
          <div className="graph-container">
            {/*
              The key parameter was added to fix the error "Duplicate id was found" when rendering new graph
              with new sets of nodes and edges with React.StrictMode
            */}
            <Graph getNetwork={setNetworkInstance} graph={graph} options={options} events={events} />
          </div>
        </Col>
      </Row>
    );
  };

  const renderInvalidTicket = () => {
    return (
      <div className="type-headline-60">Ticket Does Not Exist, Or you do not have permission to view it</div>
    );
  }

  const renderLoadingState = () => {
    return (
      <VDSLoading size="icon-size-50" />
    );
  }


  const renderGraph = () => {
    if (isLoading) {
      return renderLoadingState();
    } else {
      if (isValid) {
        return renderValidTicket();
      } else {
        return renderInvalidTicket();
      }
    }
  }

  return (
    <div className="container">
      <div className="type-headline-40">
        Ticket Visualization
      </div>
      <div className="inline-project-form">
        <ProjectSelectionForm
          onFinish={onFinish} 
          selectedTicket={selectedTicket}
          layout={'inline'}
        />
      </div>
      <Row>
        <LineTypesTable />
        <ColorNodeTable />
      </Row>
      {renderGraph()}
      
      <TicketEditModal 
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        selectedNode={selectedNode}
        selectedNodeId={selectedNodeId}
        selectedTicketId={selectedTicketId}
        graph={graph}
        updateGraph={updateGraph}
      />
    </div>
  );
}

export default Visualization;