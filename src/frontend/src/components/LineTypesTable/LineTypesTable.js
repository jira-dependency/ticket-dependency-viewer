import React from 'react';
import { Row, Col } from 'antd';
import './LineTypesTable.css';
import Graph from "react-graph-vis";
import * as constants from '../../utils/constants';
import { v4 as uuidv4 } from 'uuid';

const LineTypesTable = () => {
  const TABLE_SPAN = 12;
  const COL_SPAN = 12;

  const options = {
    autoResize: false,
    layout: {
      hierarchical: true
    },
    interaction: {
      dragNodes: false,
      dragView: false,
      selectable: false,
      zoomView: false
    },
    edges: {
      physics: false,
      arrows: {
        to: {
          enabled: false
        }
      }
    }
  };

  const generateLineTypesTable = () => {
    const table  = Object.entries(constants.EDGE_STYLE).map(([key, value]) => {
      const nodeList = [
        { id: 1, level: 1, color: 'rgba(0, 0, 0, 0)' },
        { id: 2, level: 1, color: 'rgba(0, 0, 0, 0)' }
      ];
      const edgeList = [
        { from: 1, to: 2, ...value }
      ];
      const graph = {
        nodes: nodeList,
        edges: edgeList
      };
      const label = constants.EDGE_STATUS[key].join(', ');
      return (
        <Row>
          <Col span={COL_SPAN} className="line-type-column-container">
            <div className="sample-container">
              <Graph key={uuidv4()} graph={graph} options={options} />
            </div>
            
          </Col>
          <Col span={COL_SPAN} className="line-type-column-container">
            <div className="type-body-default">{label}</div>
          </Col>
        </Row>
      );
    })
    return table;
  }
  
  return (
    <Col span={TABLE_SPAN}>
      <div className="type-headline-80">
        Line types
      </div>
      <div className="types-table-container">
        <Row>
          <Col span={COL_SPAN}>
            <div className="type-headline-90">Line style</div>
          </Col>
          <Col span={COL_SPAN}>
            <div className="type-headline-90">Link type</div>
          </Col>
        </Row>
        {generateLineTypesTable()}
      </div>
    </Col>
  );
};

export default LineTypesTable;