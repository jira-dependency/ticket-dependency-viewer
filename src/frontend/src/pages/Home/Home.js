import React from 'react';
import "../pages.css";
import "./Home.css";
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { VDSButton } from '@vidmob/vidmob-design-system';

const Home = () => {
  const history = useHistory();
  /* 
  Log the user in if they provide the correct email
  params:
    - email (string): the user's email
  */
  const login = () => {
    /* This is the latest api from the backend but I will update the request after
    Eric's pr merge */
    axios.post('http://local.vidmob.com:5000/login')
      .then((res) => {
        history.push("/project-select");
      })
      .catch((err) => console.error(err));
  };

  const onFinish = (values) => {
    login();
  };

  return (
    <div className="container">
      <div className="type-headline-40">
        Ticket Dependency Viewer
      </div>
      <div style={{ marginTop: '20px' }}>
        <VDSButton label="Login" onClick={onFinish} />
      </div>
    </div>
  );
};

export default Home;