import React from 'react';
import { Form } from 'antd';
import { VDSButton, VDSTextField } from '@vidmob/vidmob-design-system';
import './ProjectSelectionForm.css';

const ProjectSelectionForm = ({ onFinish, selectedTicket, layout }) => {
  const [form] =  Form.useForm();

  return (
    <Form
      form={form}
      onFinish={onFinish}
      className="project-select-container"
      layout={layout}
      initialValues={{
        ticketId: selectedTicket.ticketId === '' ? undefined : selectedTicket.ticketId,
        depth: selectedTicket.depth
      }}
    >
      <Form.Item className="project-selection-form-item">
        <div className="type-headline-90">
          Find your ticket
        </div>
      </Form.Item>
      <Form.Item className="project-selection-form-item">
        <div className="type-body-strong">
          Ticket ID:
        </div>
      </Form.Item>
      <Form.Item name="ticketId" rules={[{ required: true, message: 'Please put in your ticket ID!' }]}>
        <VDSTextField
          className="select-input"
          placeholder="Ticket ID"
        />
      </Form.Item>
      <Form.Item className="project-selection-form-item">
        <div className="type-body-strong">
          Depth:
        </div>
      </Form.Item>
      <Form.Item
        name="depth"
        rules={[
          {
            required: true,
            message: 'Please enter a depth'
          },
          () => ({
            validator(_, value) {
              if (parseInt(value) < 1 || parseInt(value) > 5) {
                return Promise.reject('Please put in a depth from 1 to 5');
              }
              return Promise.resolve();
            }
          })
        ]}
      >
        <VDSTextField
          className="select-input"
          placeholder="Depth"
        />
      </Form.Item>
      <Form.Item className="project-selection-form-item">
        <VDSButton
          label="Search"
          iconName="search"
          reverse
        />
      </Form.Item>
    </Form>
  );
};

export default ProjectSelectionForm;