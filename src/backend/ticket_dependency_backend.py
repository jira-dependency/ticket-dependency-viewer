from flask import Flask, abort
from flask_restful import Api, Resource, request
from flask_cors import CORS
import endpoint_controller

app = Flask(__name__)
CORS(app)
api = Api(app)

#the login route to connect the user to the program
app.add_url_rule('/login', methods=['POST'], view_func=endpoint_controller.user_login)

#the ticket route that returns an issue's data model to the frontend
app.add_url_rule('/ticket', methods=['POST'], view_func=endpoint_controller.get_ticket)

#the createlink route that creates a new requested issuelink
app.add_url_rule('/createlink', methods=['POST'], view_func=endpoint_controller.create_link)

#the full depth relationship route for full web of related issues past depth 1
app.add_url_rule('/fullDepth', methods=['POST'], view_func=endpoint_controller.get_full_depth)

#the update issue endpoint that allows a user to update an issue's fields directly from the app
app.add_url_rule('/updateIssue', methods=['POST'], view_func=endpoint_controller.update_issue)

#the project endpoint that returns active issues from a requested project
app.add_url_rule('/project', methods=['POST'], view_func=endpoint_controller.get_project)

#the epic endpoint that returns active issues that are part of the requested epic
app.add_url_rule('/epic', methods=['POST'], view_func=endpoint_controller.get_epic)

if __name__ == "__main__":
    app.run(debug=True, use_reloader=True, host='0.0.0.0')
