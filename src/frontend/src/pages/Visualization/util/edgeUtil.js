import * as constants from '../../../utils/constants';

const BLOCK_STATUS = 'blocks';
const BLOCKED_BY_STATUS = 'is blocked by';
const CLONE_STATUS = 'clones';
const CLONED_BY_STATUS = 'is cloned by';
const RELATES_STATUS = 'relates to';
const DUPLICATES_STATUS = 'duplicates';
const DUPLICATED_BY_STATUS = 'is duplicated by';
const SPLIT_TO_STATUS = 'split to';
const SPLIT_FROM_STATUS = 'is split from';
const CAUSE_STATUS = 'causes';
const CAUSED_BY_STATUS = 'is caused by';
const RELEASE_STATUS = 'releases';
const RELEASED_BY_STATUS = 'is released by';
const BREAK_TO_STATUS = 'breaks out to';
const BREAK_OUT_STATUS = 'broken out from';
const SOLVE_STATUS = 'solves';
const SOLVED_BY_STATUS = 'is solved by';
const TEST_STATUS = 'tests';
const TESTED_BY_STATUS = 'is tested by';

export const generateEdgeList = (ticketsArray, ticketNodesMap) => {
  const edgesList = [];
  ticketsArray.forEach((ticketInfo) => {
    const issueLinkMap = getIssueLinkMap(ticketInfo);
    for (const [key, value] of issueLinkMap) {
      value.ticketIdList.forEach((ticketId) => {
        const ticketEdge = generateEdge(ticketId, value.inDirection, key, ticketInfo.key, ticketNodesMap);
        if (ticketEdge != null) {
          value.edgeList.push(ticketEdge);
        }
      });
      if (value.edgeList.length > 0) {
        value.edgeList.forEach((value) => {
          const contained = edgesList.find(edge => JSON.stringify(edge) === JSON.stringify(value));
          if (contained === undefined) {
            edgesList.push(value);
          }
        });
      }
    }
  });
  return edgesList;
};

const getIssueLinkMap = (ticketInfo) => {
  const issueLinkMap = new Map();
  issueLinkMap.set(BLOCK_STATUS, { ticketIdList: ticketInfo.issuelinks.blocks, edgeList: [], inDirection: false });
  issueLinkMap.set(BLOCKED_BY_STATUS, { ticketIdList: ticketInfo.issuelinks["is blocked by"], edgeList: [], inDirection: true });
  issueLinkMap.set(CLONE_STATUS, { ticketIdList: ticketInfo.issuelinks.clones, edgeList: [], inDirection: false });
  issueLinkMap.set(CLONED_BY_STATUS, { ticketIdList: ticketInfo.issuelinks["is cloned by"], edgeList: [], inDirection: true });
  issueLinkMap.set(RELATES_STATUS, { ticketIdList: ticketInfo.issuelinks["relates to"], edgeList: [], inDirection: false });
  issueLinkMap.set(DUPLICATES_STATUS, { ticketIdList: ticketInfo.issuelinks.duplicates, edgeList: [], inDirection: false });
  issueLinkMap.set(DUPLICATED_BY_STATUS, { ticketIdList: ticketInfo.issuelinks["is duplicated by"], edgeList: [], inDirection: true });
  issueLinkMap.set(CAUSED_BY_STATUS, { ticketIdList: ticketInfo.issuelinks["is caused by"], edgeList: [], inDirection: true });
  issueLinkMap.set(CAUSE_STATUS, { ticketIdList: ticketInfo.issuelinks.causes, edgeList: [], inDirection: false });
  issueLinkMap.set(SPLIT_TO_STATUS, { ticketIdList: ticketInfo.issuelinks["split to"], edgeList: [], inDirection: false });
  issueLinkMap.set(SPLIT_FROM_STATUS, { ticketIdList: ticketInfo.issuelinks["is split from"], edgeList: [], inDirection: true });
  issueLinkMap.set(RELEASE_STATUS, { ticketIdList: ticketInfo.issuelinks.releases, edgeList: [], inDirection: false });
  issueLinkMap.set(RELEASED_BY_STATUS, { ticketIdList: ticketInfo.issuelinks["is released by"], edgeList: [], inDirection: true });
  issueLinkMap.set(BREAK_TO_STATUS, { ticketIdList: ticketInfo.issuelinks["breaks out to"], edgeList: [], inDirection: true });
  issueLinkMap.set(BREAK_OUT_STATUS, { ticketIdList: ticketInfo.issuelinks["is broken out from"], edgeList: [], inDirection: false });
  issueLinkMap.set(SOLVE_STATUS, { ticketIdList: ticketInfo.issuelinks.solves, edgeList: [], inDirection: false });
  issueLinkMap.set(SOLVED_BY_STATUS, { ticketIdList: ticketInfo.issuelinks["is solved by"], edgeList: [], inDirection: true });
  issueLinkMap.set(TEST_STATUS, { ticketIdList: ticketInfo.issuelinks.tests, edgeList: [], inDirection: false });
  issueLinkMap.set(TESTED_BY_STATUS, { ticketIdList: ticketInfo.issuelinks["is tested by"], edgeList: [], inDirection: true });
  return issueLinkMap
};

const generateEdge = (ticketId, inDirection, edgeStatus, currTicketId, ticketNodesMap) => {
  const destTicketId = ticketNodesMap.has(ticketId) ? ticketNodesMap.get(ticketId).id : null;
  const srcTicketId = ticketNodesMap.has(currTicketId) ? ticketNodesMap.get(currTicketId).id : null;
  if (destTicketId != null && srcTicketId != null) {
    let tempEdge = {
      from: srcTicketId,
      to: destTicketId,
      ...constants.EDGE_CONFIG
    };
    if (inDirection) {
      tempEdge = {
        from: destTicketId,
        to: srcTicketId,
        ...constants.EDGE_CONFIG
      }
    }
    Object.entries(constants.EDGE_STATUS).forEach(([key, value]) => {
      if (value.includes(edgeStatus)) {
        tempEdge = { ...tempEdge, ...constants.EDGE_STYLE[key] };
      }
    });
    return tempEdge;
  }
  return null;
};