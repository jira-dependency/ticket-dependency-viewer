from jira import JIRA
import jira_config as cfg
import constants
#Log into Jira
#requires email and API key
def login():
    return JIRA(basic_auth=(cfg.EMAIL, cfg.API_KEY), options={'server':constants.VIDMOB_SERVER})
