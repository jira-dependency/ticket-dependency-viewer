# vidmob-ticket-dependency-viewer

Team Tartans intern project - display dependency chains / trees of jira tickets

# Installation - For Linux/MacOS Users
# Go to bottom for Windows Users installation

Before running, create a file named 'jira_config.py' in the /backend directory:

The file 'jira_config.py' is already in .gitignore

```
API_KEY = '<your_jira_api_key>'
EMAIL = '<your_email>'
```

To use Docker you will also need to create a `.npmrc` file in the /frontend directory containing your gitlab API key.  This needs to be inside the repository for the Docker image to work; your `~.npmrc` will not be readable by Docker:

```
engine-strict=true
@vidmob:registry=https://gitlab.com/api/v4/packages/npm/\
//gitlab.com/api/v4/packages/npm/:_authToken=<your_gitlab_api_key>
```

# Running (with Docker)

Build and run a docker image thusly: 

```
docker build -t tic .
docker run -d tic
```

("tic" is the docker image name; you can use anything you like there)

# Running (without Docker)

The following python packages need to be installed:
```
pip3 install flask
pip3 install jira
pip3 install requests
pip3 install flask_restful
pip3 install flask_cors
```

Then run `src/startapp.sh` to start both the frontend and backend servers.

# WINDOWS USERS

# Running (without Docker)

The following python packages need to be installed:
```
pip3 install flask
pip3 install jira
pip3 install requests
pip3 install flask_restful
pip3 install flask_cors
```

Navigate to \ticket-dependency-viewer\src and run the following command

```
python3 ./backend/ticket_dependency_backend.py

```

Open up a second terminal and navigate to \ticket-dependency-viewer\src\frontend and run the following commands

```
npm ci
npm start

```

The app will now be running on localhost:3000


