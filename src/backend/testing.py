import build_data_model as Model
import login as Login
import constants
import connection
import jira_api_service as JiraApi
import ticket_data_retrieval

from jira import JIRA
import jira_config as cfg

#might receive request for specific project/sprint to pull data from
if __name__ == "__main__":
    connection.jira = Login.login()
    all_issues = JiraApi.get_all_issues(connection.jira, 'TIC-37')
    requested_issue = JiraApi.get_issue(connection.jira, 'AGL-8257')
    other_requested_issue = JiraApi.get_issue(connection.jira, 'AGL-8281')
    print(ticket_data_retrieval.get_type(requested_issue.raw[constants.FIELDS][constants.ISSUETYPE]))

    print(JiraApi.get_all_issues_in_epic(connection.jira, 'AGL-8281'))
    #ticket_data_retrieval.get_all_related_issues('TIC-17', 1)

    #ticket_data_retrieval.get_all_related_issues('TIC-27', 2)
    #the following code can be uncommented in order for CMD to display
    #all the fields/values of an issue

    #if requested_issue is not None:
        #print(requested_issue.raw)
        #for field_name in requested_issue.raw['fields']:
            #if(requested_issue.raw['fields'][field_name] is not None):
                #print("Field:", field_name, "Value:", requested_issue.raw['fields'][field_name])

    #print(JiraApi.get_project_issues(connection.jira, 'TIC'))
