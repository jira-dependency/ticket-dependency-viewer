import build_data_model as Model
import ticket_data_retrieval
import login as Login
from flask import Flask, abort
from flask_restful import Api, Resource, request
from flask_cors import CORS
import requests
import jira_config as cfg
from jira import JIRA
import constants
import connection
import jira_api_service as JiraApi

#the login endpoint to connect the user to the program
def user_login():
    vidmob = 'vidmob.com'
    try:
        domain = cfg.EMAIL.split('@')[1]
    except LookupError:
        print("Email field missing or invalid")
        return abort(400)

    if(domain == vidmob):
        connection.jira = Login.login()
        return {"user logged in": cfg.EMAIL}
    return abort(400)

#the ticket endpoint that returns an issue's data model to the frontend
def get_ticket():
    if(connection.jira is None):
        return {"ERROR": "Not logged in"}
    try:
        ticket_id = request.json[constants.TICKET_FIELD]
        proj_id = ticket_id.split('-')[0]
        requested_issue = JiraApi.get_issue(connection.jira, ticket_id)
    except LookupError:
        print("ticket field in request missing or invalid")
        return abort(400)

    if(requested_issue is not None):
        built_ticket = Model.build_data_model(requested_issue)
        return built_ticket
    else:
        return abort(400)

#the createlink endpoint that creates a new requested issuelink
def create_link():
    if(connection.jira == None):
        return {"ERROR": "Not logged in"}
    try:
        issue_link_type = request.json[constants.LINK_TYPE_FIELD]
        ticket_to_link_from = request.json[constants.LINK_FROM_FIELD]
        ticket_to_link_to = request.json[constants.LINK_TO_FIELD]
        result = JiraApi.create_link(connection.jira, issue_link_type, ticket_to_link_from, ticket_to_link_to)
    except LookupError:
        print("Request fields  for IssueLinks missing or invalid")
        return abort(400)

    if(result == 201):
        return {"link creation": "success"}
    else:
        return abort(400)

#get the full depth tree/graph of all related issue nodes
def get_full_depth():
    if(connection.jira == None):
        return {"ERROR": "Not logged in"}
    try:
        ticket_id = request.json[constants.TICKET_FIELD]
        depth = request.json[constants.DEPTH_FIELD]
        requested_issue = JiraApi.get_issue(connection.jira, ticket_id)
    except LookupError:
        print("ticket field in request missing or invalid")
        return abort(400)

    if(requested_issue is not None):
        model = ticket_data_retrieval.get_all_related_issues(requested_issue.raw['key'], depth)
        return model
    else:
        return abort(400)

#update an issue's fields with the given field/value Dict
def update_issue():
    if(connection.jira is None):
        return {"ERROR": "Not logged in"}
    try:
        ticket_id = request.json[constants.TICKET_FIELD]
        updated_fields = request.json['fields']
        JiraApi.update_issue(connection.jira, ticket_id, updated_fields)
        return {"Issue Update": "Success"}

    except LookupError:
        print("ticket field in request missing or invalid")
        return abort(400)

#return all active issues of the requested project
def get_project():
    if(connection.jira is None):
        return {"ERROR": "Not logged in"}
    try:
        project_id = request.json[constants.PROJECT_FIELD]
        issues = JiraApi.get_project_issues(connection.jira, project_id)
        return issues
    except:
        return abort(400)

def get_epic():
    if(connection.jira is None):
        return {"ERROR": "Not logged in"}
    try:
        epic_id = request.json[constants.TICKET_FIELD]
        epic_issue = JiraApi.get_issue(connection.jira, epic_id)
        if(ticket_data_retrieval.get_type(epic_issue.raw[constants.FIELDS][constants.ISSUETYPE]) != 'Epic'):
            return {"ERROR": "Requested issue is not an EPIC"}
        issues = JiraApi.get_all_issues_in_epic(connection.jira, epic_id)
        return issues
    except:
        return abort(400)
