export const STATUS_COLOR = [
  {
    status: ['to do'],
    color: '#42526E',
    className: 'to-do-container'
  },
  {
    status: ['in progress', 'design review', 'functional review', 'code review', 'deployed to production', 'deployed to stage', 'tested on stage'],
    color: '#0747A6',
    className: 'review-container'
  },
  {
    status: ['merged for release', 'releasing', 'done', 'tested on production'],
    color: '#006644',
    className: 'merged-container'
  }
];

export const ISSUE_TYPE = {
  'task': { code: '\ue913', color: 'rgb(76, 172, 236)' },
  'bug': { code: '\ue90e', color: 'rgb(228, 76, 60)' },
  'epic': { code: '\ue909', color: 'rgb(148, 76, 228)' },
  'story': { code: '\ue902', color: 'rgb(100, 188, 60)' },
}

export const NODE_CONFIG = {
  font: {
    size: 13,
    color: '#FFFFFF',
    multi: true,
    face: "icomoon"
  },
  shape: 'box',
  scaling: {
    max: 8
  },
  heightConstraint: 30,
};

export const EDGE_CONFIG = {
  length: 300,
};

export const EDGE_STYLE = {
  BLOCKS: { width: 2 },
  CLONES: { dashes: [10, 6], width: 2 },
  DUPLICATES: { dashes: true, width: 2 },
  SPLITS: { dashes: [0.5, 4], width: 2 },
  CAUSES: { dashes: [15, 15], width: 2 },
  RELATES_TO: { dashes: [8, 15], width: 2 },
  RELEASES: { dashes: [0.5, 10], width: 2 },
  BREAKS_OUT: { dashes: [20, 10], width: 2 },
  SOLVES: { dashes: [30, 5], width: 2 },
  TESTS: { dashes: [1, 20], width: 2 },
}

export const EDGE_STATUS = {
  BLOCKS: ['blocks', 'is blocked by'],
  CLONES: ['clones', 'is cloned by'],
  DUPLICATES: ['duplicates', 'is duplicated by'],
  SPLITS: ['split to', 'is split from'],
  CAUSES: ['causes', 'is caused by'],
  RELATES_TO: ['relates to'],
  RELEASES: ['releases', 'is released by'],
  BREAKS_OUT: ['breaks out to', 'is broken out from'],
  SOLVES: ['solves', 'is solved by'],
  TESTS: ['tests', 'is tested by']
}

/* The reason why 2 is our default depth value is because we think that this is the lowest that
whe can go and the graph can still give some valuable insights. */
export const MIN_DEPTH = 2;
