import json
import constants
import ticket_data_retrieval as TicketData

#Return all of the fields that we want from the requested issue
def build_data_model(requested_issue, requested_fields = constants.DEFAULT_TICKET_FIELDS):

    #model to be built and translated into JSON object
    model = {}
    model[constants.KEY] = requested_issue.raw[constants.KEY]
    model[constants.POINTS_BLOCKED] = 0
    model[constants.LONGEST_CHAIN_BLOCKED] = 0
    for field_name in requested_issue.raw['fields']:
        if(field_name in requested_fields):

            #Check for a dictionary or list so that we can parse further
            #any other value will be single and returned with no extra parsing
            field_data = requested_issue.raw[constants.FIELDS][field_name]
            if(isinstance(field_data, dict) or isinstance(field_data, list)):
                #parse issue type field to retrive type of issue
                if field_name == constants.ISSUETYPE:
                    model[field_name] = TicketData.get_type(field_data)
                #parse priority field to retrieve wanted information
                if field_name == constants.PRIORITY:
                    model[field_name] = TicketData.get_priority(field_data)

                #parse assignee field to retrieve wanted information
                if field_name == constants.ASSIGNEE:
                    model[field_name] = TicketData.get_assignee(field_data)
                    model[constants.EMAIL] = TicketData.get_email(field_data)

                #parse status field to retrieve wanted information
                if field_name == constants.STATUS:
                    model[field_name] = TicketData.get_status(field_data)

                #parse issuelinks field to retrieve wanted information
                if(field_name == constants.ISSUELINKS):
                    relationship_lists = TicketData.get_relationship_lists(field_data)
                    model_issuelinks = {}
                    model_issuelinks['blocks']             = relationship_lists[0]
                    model_issuelinks['is blocked by']      = relationship_lists[1]
                    model_issuelinks['clones']             = relationship_lists[2]
                    model_issuelinks['is cloned by']       = relationship_lists[3]
                    model_issuelinks['duplicates']         = relationship_lists[4]
                    model_issuelinks['is duplicated by']   = relationship_lists[5]
                    model_issuelinks['relates to']         = relationship_lists[6]
                    model_issuelinks['split to']           = relationship_lists[7]
                    model_issuelinks['is split from']      = relationship_lists[8]
                    model_issuelinks['causes']             = relationship_lists[9]
                    model_issuelinks['is caused by']       = relationship_lists[10]
                    model_issuelinks['releases']           = relationship_lists[11]
                    model_issuelinks['is released by']     = relationship_lists[12]
                    model_issuelinks['breaks out to']      = relationship_lists[13]
                    model_issuelinks['is broken out from'] = relationship_lists[14]
                    model_issuelinks['solves']             = relationship_lists[15]
                    model_issuelinks['is solved by']       = relationship_lists[16]
                    model_issuelinks['tests']              = relationship_lists[17]
                    model_issuelinks['is tested by']       = relationship_lists[18]
                    model[field_name] = model_issuelinks

                sprint_and_board_data = constants.SPRINT_AND_BOARD
                if(field_name == sprint_and_board_data):
                    model[constants.SPRINT_DATA] = TicketData.get_sprint(field_data)
                    model[constants.BOARDID] = TicketData.get_board(field_data)

            #If value held in field is a single value, return without parsing
            else:
                story_points = constants.POINTS
                if(field_name == story_points):
                    model[constants.STORY_POINTS] = TicketData.get_points(field_data)
                else:
                    model[field_name] = field_data

    #Translate data model into JSON object
    print(model)
    return model
