import React, { useEffect, useState } from 'react';
import { Modal, Form, Input } from 'antd';
import axios from 'axios';
import * as constants from '../../pages/Visualization/util/nodeUtil';
import { VDSButton, VDSLoading, VDSTextField } from '@vidmob/vidmob-design-system';
import './TicketEditModal.css';

const { TextArea } = Input;

const TicketEditModal = ({ isModalVisible, setIsModalVisible, selectedNodeId, selectedNode, selectedTicketId, graph, updateGraph }) => {
  const [form] = Form.useForm();
  const [error, setError] = useState('');
  const [selectedNodeInfo, setSelecetedNodeInfo] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleSubmit = (values) => {
    setIsSubmitting(true);
    const desc = values.desc;
    const summary = values.summary;
    const points = values.points === '' ? 0 : parseInt(values.points);
    axios.post('http://local.vidmob.com:5000/updateIssue', {
      ticket: selectedNodeId,
      fields: {
        summary: summary,
        description: desc,
        customfield_10005: points
      }
    })
      .then(() => {
        const oldDesc = selectedNodeInfo.description ? selectedNodeInfo.description : 'None';
        const oldSummary = selectedNodeInfo.summary ? selectedNodeInfo.summary : 'None';
        const oldPoints = selectedNodeInfo["story points"] ? selectedNodeInfo["story points"] : 0;
        if (oldPoints !== points) {
          const labelArr = selectedNode.label.split('\n');
          labelArr[2] = 'Points: ' + points;
          const newLabel = labelArr.join('\n');
          selectedNode.label = newLabel;
        }
        if (summary !== oldSummary || desc !== oldDesc) {
          const priority = selectedNodeInfo.priority ? selectedNodeInfo.priority : 'None';
          const sprint = selectedNodeInfo.sprint_data ? selectedNodeInfo.sprint_data[0] : 'None';
          const longestChainBlocked = selectedNodeInfo.longest_chain_blocked;
          const pointsBlocked = selectedNodeInfo.points_blocked;
          selectedNode.title = constants.buildNodeTitle(desc, summary, priority, sprint, longestChainBlocked, pointsBlocked);
        }
        const newNodeList = graph.nodes.map((item) => {
            if (item.id === selectedTicketId) {
              return selectedNode;
            }
            return item;
          }
        );
        const newGraph = { ...graph };
        newGraph.nodes = newNodeList;
        updateGraph(newGraph);
        setIsModalVisible(false);
      })
      .catch((err) => {
        setError(err);
      });
  }

  useEffect(() => {
    if (selectedNodeId !== '') {
      axios.post('http://local.vidmob.com:5000/ticket', {
          ticket: selectedNodeId
        })
          .then((res) => {
            setSelecetedNodeInfo(res.data);
            setIsLoading(false);
          })
          .catch((err) => {
            console.error(err);
          });
    }
  }, [selectedNodeId])

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <Modal
      title={<div className="type-headline-90">Ticket Information</div>} 
      visible={isModalVisible}
      onCancel={handleCancel}
      footer={[
        <VDSButton label="Cancel" onClick={handleCancel} type="basic" />,
        <VDSButton 
          label="Submit"
          loading={isSubmitting}
          onClick={() => {
            form
              .validateFields()
              .then(values => {
                handleSubmit(values);
                setIsSubmitting(false);
              })
              .catch(info => {
                console.error('Validate Failed:', info);
              });
          }} 
        />
      ]}
    >
      {!isLoading
        ? (
          <Form
            form={form}
            layout="vertical"
            initialValues={{
              summary: selectedNodeInfo !== null ? selectedNodeInfo.summary : "",
              desc: selectedNodeInfo !== null ? selectedNodeInfo.description : "",
              points: selectedNodeInfo !== null ? selectedNodeInfo["story points"] : "",
            }}
          >
            <Form.Item 
              label={<div className="type-body-large-default">Summary</div>}
              name="summary"
            >
              <VDSTextField />
            </Form.Item>
            <Form.Item
              label={<div className="type-body-large-default">Description</div>}
              name="desc"
            >
              <TextArea rows={4} />
            </Form.Item>
            <Form.Item
              label={<div className="type-body-large-default">Points</div>}
              name="points"
            >
              <VDSTextField />
            </Form.Item>
            {error !== '' && <h5>{error}</h5>}
          </Form>
        ) : (
          <div className="modal-loading">
            <VDSLoading size="icon-size-30" />
          </div>
        )
      }
    </Modal>
  );
};

export default TicketEditModal;