import * as constants from '../../../utils/constants';

export const generateNodeList = (ticketsArray, ticketNodesMap) => {
  const ticketList = [];
  let ticketId = 1;
  ticketsArray.forEach((ticketInfo) => {
    const ticket = generateNode(ticketId, ticketInfo);
    ticketNodesMap.set(ticketInfo.key, ticket);
    ticketList.push(ticket);
    ticketId++;    
  });
  return ticketList;
};

const generateNode = (ticketId, ticketInfo) => {
  let tempNode = {...constants.NODE_CONFIG};
  const assignee = ticketInfo.assignee ? ticketInfo.assignee : 'None';
  const points = ticketInfo["story points"] ? ticketInfo["story points"] : 0;
  let node = generateNodeLabel(tempNode, ticketInfo.issuetype.toLowerCase(), ticketInfo.key, assignee, points);
  const status = ticketInfo.status ? ticketInfo.status : 'None';
  const desc = ticketInfo.description ? ticketInfo.description : 'None';
  const summary = ticketInfo.summary ? ticketInfo.summary : 'None';
  const priority = ticketInfo.priority ? ticketInfo.priority : 'None';
  const sprint = ticketInfo.sprint_data ? ticketInfo.sprint_data[0] : 'None';
  const longestChainBlocked = ticketInfo.longest_chain_blocked;
  const pointsBlocked = ticketInfo.points_blocked;
  node.id = ticketId;
  node.title = buildNodeTitle(desc, summary, priority, sprint, longestChainBlocked, pointsBlocked);
  return matchColorToStatus(node, status);
};

const matchColorToStatus = (node, ticketStatus) => {
  const nodeStatus = constants.STATUS_COLOR.find((statusEntry) => {
    const nodeStatus = statusEntry.status.find((status) => ticketStatus.toLowerCase().includes(status));
    return nodeStatus
  });
  if (nodeStatus) {
    node.color = nodeStatus.color;
  }
  return node;
};

export const generateNodeLabel = (node, issueType, nodeId, assignee, points) => {
  let label = '';
  if (constants.ISSUE_TYPE.hasOwnProperty(issueType)) {
    label += nodeId + ' <b>' +constants.ISSUE_TYPE[issueType].code + '</b>';
    node.font = {...node.font, bold: {color: constants.ISSUE_TYPE[issueType].color}}
  }
  label += `\nAssignee: ${assignee}\nPoints: ${points}`;
  return { label: label, ...node};
}

export function buildNodeTitle(desc, summary, priority, sprint, longestChainBlocked, pointBlocked) {
  const titleObj = {
    'Description': desc,
    'Summary': summary,
    'Priority': priority,
    'Sprint': sprint,
    'Number of tickets are blocked': longestChainBlocked,
    'Number of points are blocked': pointBlocked,
  };
  let title = '';
  for (const key in titleObj) {
    title += `<p style='white-space: pre-line;'><b>${key}:</b> ${titleObj[key]}</p>`;
  }
  const container = document.createElement("div");
  container.setAttribute("style", "width: 400px; text-align: left;");
  container.innerHTML = title;
  return container;
}