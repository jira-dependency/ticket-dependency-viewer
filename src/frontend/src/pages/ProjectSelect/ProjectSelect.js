import React, { useContext } from 'react';
import "../pages.css";
import "./ProjectSelect.css";
import selectionContext from '../../utils/context/selectionContext';
import { useHistory } from 'react-router-dom';
import { ProjectSelectionForm } from '../../components/ProjectSelectionForm';

const ProjectSelect = () =>  {
  const { selectedTicket, setSelectedTicket } = useContext(selectionContext);
  const history = useHistory();
  // Placeholder form onSubmit function
  const onFinish = (values) => {
    const ticket = {
      ticketId: values.ticketId,
      depth: parseInt(values.depth)
    }
    setSelectedTicket(ticket);
    history.push('/visualization');
  };

  return (
    <div className="container">
      <ProjectSelectionForm
        onFinish={onFinish} 
        selectedTicket={selectedTicket}
        layout={'horizontal'}
      />
    </div>
  );
}

export default ProjectSelect;
