import jira_config as cfg
from jira import JIRA

FIELDS = 'fields'
EMAIL = 'email'
TICKET_FIELD = 'ticket'
PROJECT_FIELD = 'project'
DEPTH_FIELD = 'depth'
USER_EMAIL = 'email'
LINK_TYPE_FIELD = 'link type'
LINK_FROM_FIELD = 'link from'
LINK_TO_FIELD = 'link to'
BLOCKS = 'blocks'
POINTS_BLOCKED = 'points_blocked'
LONGEST_CHAIN_BLOCKED = 'longest_chain_blocked'

LINK_TYPES = ['is blocked by', 'blocks', 'is cloned by', 'clones', 'is duplicated by',
 'duplicates', 'split from', 'split to', 'is caused by', 'causes', 'relates to', 'is released by',
  'releases', 'broken out from', 'breaks out to', 'is solved by', 'solves', 'is tested by', 'tests']
#default argument for build_data_model
#customfield_10007 holds the sprint and board data
#customfield_10005 holds the story point value
SPRINT_DATA = 'sprint_data'
BOARDID = 'boardId'
STORY_POINTS = 'story points'

PRIORITY = 'priority'
ASSIGNEE = 'assignee'
STATUS = 'status'
DESCRIPTION = 'description'
SPRINT_AND_BOARD = 'customfield_10007'
SUMMARY = 'summary'
ISSUELINKS = 'issuelinks'
ISSUETYPE = 'issuetype'
KEY = 'key'
POINTS = 'customfield_10005'
DEFAULT_TICKET_FIELDS = [PRIORITY, ASSIGNEE, STATUS, DESCRIPTION, SPRINT_AND_BOARD, SUMMARY, ISSUELINKS, ISSUETYPE, KEY, POINTS]

ZERO = 0
CHUNK_SIZE = 50

VIDMOB_SERVER = 'https://vidmob.atlassian.net'
